#include <Application.hpp>
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include <StraightCameraMover.h>

#include <iostream>
#include <vector>

#include "Maze.h"

class SampleApplication : public Application
{
public:
	MeshPtr _maze;

    CameraMoverPtr free_cam = std::make_shared<FreeCameraMover>();
    CameraMoverPtr straight_cam = std::make_shared<FreeCameraMover>();

	std::vector<ShaderProgramPtr> _shaders;

	void makeScene() override
	{
		Application::makeScene();
		MazePtr maze = std::make_shared<Maze>(4, 4, "/home/pavel/gfx/tasks/task1/497Strokan/maze.txt");
        straight_cam = std::make_shared<StraightCameraMover>(maze);
        _cameraMover = free_cam;
		_maze = maze->mazeMesh();
		_maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_F1)
			{
				_cameraMover = free_cam;
			} else if (key == GLFW_KEY_F2)
			{
				_cameraMover = straight_cam;
			}
		}
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
		}
		ImGui::End();
	}

	void draw() override
	{
		Application::draw();

		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Подключаем шейдер
		auto shader = std::make_shared<ShaderProgram>(
				"497StrokanData/shaderNormal.vert", "497StrokanData/shader.frag"
		);
		shader->use();

		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		shader->setMat4Uniform("modelMatrix", _maze->modelMatrix());

		_maze->draw();
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}