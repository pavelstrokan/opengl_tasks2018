#include <Application.hpp>
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include <StraightCameraMover.h>

#include <iostream>
#include <vector>

#include "Maze.h"

class SampleApplication : public Application
{
public:
	MeshPtr _maze;
    MeshPtr _marker; //Маркер для источника света

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;
    float _attenuation = 1.0f;

    ShaderProgramPtr shader;
    ShaderProgramPtr _markerShader;

    CameraMoverPtr free_cam = std::make_shared<FreeCameraMover>();
    CameraMoverPtr straight_cam = std::make_shared<FreeCameraMover>();

	std::vector<ShaderProgramPtr> _shaders;

    //Координаты источника света
    float _lr = 2.0;
    float _phi = 0.0;
    float _theta = 0.0; //glm::pi<float>();

    float _bunnyShininess = 128.0f;
    
	void makeScene() override
	{
		Application::makeScene();
        //=========================================================
        //Инициализация значений переменных освщения
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
        _lightSpecularColor = glm::vec3(1.0, 1.0, 1.0);

        _bunnyShininess = 128.0f;
		MazePtr maze = std::make_shared<Maze>(4, 4, "/home/pavel/gfx/tasks/task2/497Strokan/maze.txt");

        //Подключаем шейдер
        shader = std::make_shared<ShaderProgram>(
                "497StrokanData/specularAttenuationPointLightPerFragment.vert",
                "497StrokanData/specularAttenuationPointLightPerFragment.frag"
        );

        _markerShader = std::make_shared<ShaderProgram>("497StrokanData/marker.vert", "497StrokanData/marker.frag");

        straight_cam = std::make_shared<StraightCameraMover>(maze);
        _cameraMover = free_cam;

        _marker = makeSphere(0.1f);

 		_maze = maze->mazeMesh();
		_maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_F1)
			{
				_cameraMover = free_cam;
			} else if (key == GLFW_KEY_F2)
			{
				_cameraMover = straight_cam;
			}
		}
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
		}
		ImGui::End();
	}

	void draw() override
	{
		Application::draw();

		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);
        glm::vec3 lightPos = glm::vec3(0, 0, 2);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		
		shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        shader->setVec3Uniform("light.pos", lightPos);
        shader->setVec3Uniform("light.La", _lightAmbientColor);
        shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
        shader->setVec3Uniform("light.Ls", _lightSpecularColor);
        shader->setFloatUniform("light.attenuation", _attenuation);

		{
			shader->setMat4Uniform("modelMatrix", _maze->modelMatrix());
			shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _maze->modelMatrix()))));
			_maze->draw();
		}

        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), lightPos));
            _markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
            _marker->draw();
        }

	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}