//
// Created by pavel on 14.03.18.
//

#ifndef GRAPHICSSAMPLES_MAZE_H
#define GRAPHICSSAMPLES_MAZE_H

#include <fstream>
#include <vector>
#include <Mesh.hpp>

struct Maze {
    Maze();
    Maze(int n, int m, const std::string& filename);
    void addSquare(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n);
    MeshPtr mazeMesh();

    std::vector<std::string> maze_map;
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
};

using MazePtr = std::shared_ptr<Maze>;


#endif //GRAPHICSSAMPLES_MAZE_H
