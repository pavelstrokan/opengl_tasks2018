//
// Created by pavel on 14.03.18.
//

#include "Maze.h"
#include <glm/glm.hpp>
#include <iostream>

Maze::Maze() {}

// 1234 - clockwise, n - out
void Maze::addSquare(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, glm::vec3 n) {
    vertices.push_back(p1);
    vertices.push_back(p2);
    vertices.push_back(p3);

    vertices.push_back(p1);
    vertices.push_back(p3);
    vertices.push_back(p4);

    for(int i = 0; i < 6; ++i)
        normals.push_back(n);

    texcoords.push_back(glm::vec2(0, 0));
    texcoords.push_back(glm::vec2(0, 1));
    texcoords.push_back(glm::vec2(1, 0));
    texcoords.push_back(glm::vec2(1, 0));
    texcoords.push_back(glm::vec2(0, 1));
    texcoords.push_back(glm::vec2(1, 1));
}

Maze::Maze(int n, int m, const std::string& filename) {
    std::ifstream input(filename);

    maze_map.resize(n);

    for (int y = 0; y < n; ++y) {
        input >> maze_map[y];
    }

    for (int y = 0; y < n; ++y) {
        for (int x = 0; x < m; ++x) {
//            floor
            addSquare(glm::vec3(x, y, 0), glm::vec3(x + 1, y, 0),
                   glm::vec3(x + 1, y + 1, 0), glm::vec3(x, y + 1, 0),
                   glm::vec3(0, 0, 1));
//            ceiling
            addSquare(glm::vec3(x, y, 1), glm::vec3(x, y + 1, 1),
                   glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y, 1),
                   glm::vec3(0, 0, -0.5));
//            walls
            if (maze_map[y][x] == '8') {
//                left
                if (x == 0 || x > 0 && maze_map[y][x - 1] == '1')
                    addSquare(glm::vec3(x, y, 0), glm::vec3(x, y + 1, 0),
                           glm::vec3(x, y + 1, 1), glm::vec3(x, y, 1),
                           glm::vec3(0, -0.5, 0));
//                right
                if (x == m - 1 || x < m - 1 && maze_map[y][x + 1] == '1')
                    addSquare(glm::vec3(x + 1, y, 0), glm::vec3(x + 1, y, 1),
                           glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0),
                           glm::vec3(0, 1, 0));
//                upper
                if (y == 0 || y > 0 && maze_map[y - 1][x] == '1')
                    addSquare(glm::vec3(x, y, 0), glm::vec3(x, y, 1),
                           glm::vec3(x + 1, y, 1), glm::vec3(x + 1, y, 0),
                           glm::vec3(-0.2, 0, 0));
//                lower
                if (y == n - 1 || y < n - 1 && maze_map[y + 1][x] == '1')
                    addSquare(glm::vec3(x, y + 1, 0), glm::vec3(x, y + 1, 1),
                           glm::vec3(x + 1, y + 1, 1), glm::vec3(x + 1, y + 1, 0),
                           glm::vec3(1, 0, 0));
            }
        }
    }
    input.close();
}

MeshPtr Maze::mazeMesh() {

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

//    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
//    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
//    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}